import xml.etree.ElementTree as ET
from colorama import Fore, Style, init

# Инициализация colorama для автоматического сброса стилей после каждого print()
init(autoreset=True)

def process_xml(xml_string):
    try:
        # Создаем объект ElementTree из переданной строки XML
        root = ET.fromstring(xml_string)
        
        # Далее можно обрабатывать элементы дерева XML
        # Например, вывести атрибуты всех элементов:
        for elem in root.iter():
            print(f"{Fore.GREEN}Element: {elem.tag}, {Fore.YELLOW}Attributes: {elem.attrib}")
    
    except ET.ParseError as e:
        print(f"Error parsing XML (ET.fromstring(xml)): {Fore.RED}\n\t{e}")
        
# Пример XML с external entity expansion
xml_data = '<!DOCTYPE data [ <!ENTITY external_entity SYSTEM "file:///etc/passwd">]><data>  <entry>&external_entity;</entry></data>'
print(f"{Fore.CYAN}=== 1) ET.fromstring(xml) ===\n{Fore.LIGHTYELLOW_EX}{xml_data}\n")
process_xml(xml_data)

def parse_xml(xml_path):
    try:
        # Создаем объект ElementTree из переданной строки XML
        toor = ET.parse(xml_path)
        
        # Далее можно обрабатывать элементы дерева XML
        # Например, вывести атрибуты всех элементов:
        for elemnt in toor.iter():
            print(f"{Fore.GREEN}Element: {elemnt.tag}, {Fore.YELLOW}Attributes: {elemnt.attrib}")
    
    except ET.ParseError as e:
        print(f"Error parsing XML (ET.parse(xml_path)): {Fore.RED}\n\t{e}")
    
    print(f"\n\n\n{Fore.CYAN}=== 3) raw exception  ===\n{Fore.LIGHTYELLOW_EX}{xml_data}\n")
    toor = ET.parse(xml_path)

xml_path = "evil.xml"
f = open(xml_path, 'r')

print(f"\n\n\n{Fore.CYAN}=== 2) ET.parse(xml_path) ===\n{Fore.LIGHTYELLOW_EX}{f.read()}\n")
parse_xml(xml_path)